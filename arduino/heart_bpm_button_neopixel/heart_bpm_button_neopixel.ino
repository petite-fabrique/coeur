// Capteur cardiaque > Serial
// 1 Leds 3W pour visualiser le rythme cardiaque
// 1 capteur

// Sensor BPM Messages to Pure Data
// If 255 > Change mode
// Else > BPM

// Set-up low-level interrupts for most acurate BPM math.
#define USE_ARDUINO_INTERRUPTS true    
#include <PulseSensorPlayground.h>  

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Wiring
const int BUTTON = 15;
const int HEARTSENSOR = 3;
const int LED = 8;

// Sensor
int Threshold = 550;
int myBPM = 60;
int b_val, last_b_val;
PulseSensorPlayground pulseSensor;

// NeoPixel
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
const long neo_interval = 200;
boolean neo_state = false;
boolean last_neo_state = false;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(1, LED, NEO_GRB + NEO_KHZ800);

void setup() {   
  Serial.begin(9600);
  pulseSensor.analogInput(HEARTSENSOR);   
  //pulseSensor.blinkOnPulse(LED); // Attach a Led to see pulses
  pulseSensor.setThreshold(Threshold);
  pulseSensor.begin();
  pinMode(BUTTON,INPUT);

  pixels.begin();
  pixels.show(); // init to off
}

void loop() {
  
  myBPM = pulseSensor.getBeatsPerMinute();
  b_val = digitalRead(BUTTON);
  currentMillis = millis();
  
  // Get button state
  if ( b_val != last_b_val) {
    if (b_val == HIGH) Serial.write(255); // Send 255
  }

  last_b_val = b_val;
  
  // Heart sensor
  if (pulseSensor.sawStartOfBeat() && (myBPM != 255) ) {
    neo_state = true;
    //Serial.println(myBPM);
    Serial.write(myBPM);
  }

  if (currentMillis - previousMillis >= neo_interval) {
      previousMillis = currentMillis;
      if (neo_state) {
        //Serial.println("led on");
        pixels.setPixelColor(0, pixels.Color(250,0,0));
        pixels.show();
        neo_state = false;
      } else {
        //Serial.println("led off");
        pixels.setPixelColor(0, pixels.Color(0,0,0));
        pixels.show();
      }
  }
 
  delay(20); // safe 
}
